from getting_started_5_mf import main
import dask
from dask import delayed

def function_calling_nifty(x):
    main()
    return x

def get_sum(a,b):
    return a+b


## Wrapping the function calls using dask.delayed
x = delayed(function_calling_nifty)(1)
y = delayed(function_calling_nifty)(2)
z = delayed(get_sum)(x, y)

## get the result using compute method
z.compute()
