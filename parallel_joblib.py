from joblib import Parallel, delayed
from getting_started_5_mf import main

def my_fun(i):
    main() 
    return i

num = 2
Parallel(n_jobs=2)(delayed(my_fun)(i) for i in range(num))
